  

**Answer 1: Steps/Strategies for Active Listening**

-  **Focus**: Give undivided attention to the speaker, avoiding distractions.

-  **Avoid interruptions**: Let the speaker finish before responding.

-  **Use door openers**: Encourage the speaker to share more by using phrases like "Tell me more."

-  **Show with body language**: Nod, maintain eye contact, and use affirming gestures.

-  **Take notes**: Jot down important points to remember and show your attentiveness.

-  **Paraphrase and clarify**: Summarize what the speaker said to ensure understanding.

  

**Answer 2: Key Points of Reflective Listening (Fisher's Model)**

-  **Mirroring non-verbal cues**: Reflect and mimic the speaker's body language and expressions.

-  **Acknowledging emotions**: Recognize and validate the speaker's emotions.

-  **Verifying the message**: Paraphrase or restate the speaker's message to ensure comprehension.

-  **Use in critical situations**: Particularly valuable in high-stakes scenarios like air traffic control or emotionally charged conversations.

-  **Application to business/software context**: Taking detailed meeting notes, verifying accuracy, and aligning stakeholders.

  

**Answer 3: Obstacles in Listening**

- Lack of attention or interest

- Preconceived notions and biases

- Distractions

- Rapid or complex speech

- Emotional barriers

- Language or cultural barriers

- Internal distractions

- Lack of feedback or clarification

  

**Answer 4: Improving Listening**

- Be fully present and attentive.

- Minimize distractions and create a conducive environment.

- Practice active listening through nodding and non-verbal cues.

- Seek clarification and ask thoughtful questions.

- Show empathy and understand the speaker's perspective.

  

**Answer 5: Switching to Passive Communication Style**

- When afraid of rejection or facing criticism.

  

**Answer 6: Switching to Aggressive Communication Style**

- During arguments, conflicts, or attempts to dominate.

  

**Answer 7: Switching to Passive-Aggressive Communication Style**

- When feeling powerless, being teased, humiliated, or to avoid direct confrontation.

  

**Answer 8: Making Communication Assertive**

1. Self-awareness: Understand your needs, feelings, and opinions.

2. Clear and direct expression: Communicate thoughts and boundaries without blame.

3. Active listening: Listen attentively and seek understanding before responding.

4. Assertive body language: Maintain eye contact and open posture.

5. Set boundaries: Clearly establish and communicate personal limits.

6. Practice saying "no": Learn to decline without guilt.

7. Manage emotions: Stay composed and express yourself respectfully.

