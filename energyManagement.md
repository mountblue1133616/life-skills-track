# Energy Management

## Manage Energy, Not Time

## Question 1: What are the activities you do that help you relax - Calm quadrant?

**Answer 1:**

- Engaging in conversations with my mother.
- Watching a good movie.
- Indulging in painting.
- Listening to soft and melodious music.
- Taking leisurely walks and appreciating nature.
- Following a routine of showering and getting a restful night's sleep.

## Question 2: When do you find yourself getting into the Stress quadrant?

**Answer 2:**

- Feeling stressed when approaching deadlines with unfinished tasks.
- Experiencing anxiety when contemplating the uncertainty of the future.
- Engaging in self-doubt and doubting past decisions.
- Feeling uncertain about personal goals and individual accomplishments.

## Question 3: How do you recognize if you are in the Excitement quadrant?

**Answer 3:**

- Becoming completely focused on the present moment and disregarding everything else.
- Feeling a sense of pride and accomplishment when achieving a goal that was once considered impossible.
- Experiencing satisfaction after completing a painting or acquiring a new skill.
- Feeling joy and enthusiasm while cooking and creating something delicious.
- Being eager and enthusiastic about learning new things, particularly in areas where personal growth and improvement are possible.

## Sleep is Your Superpower

## Question 4: Paraphrase the "Sleep is Your Superpower" video in detail.

**Answer 4:**

- Inadequate sleep has adverse effects on reproductive health in both males and females.
- Sufficient sleep is vital for optimal learning and memory functions in the brain.
- Sleep plays a crucial role in memory consolidation, both before and after learning.
- Insufficient sleep disrupts the consolidation of memories and hampers the acquisition of new information.
- The brain's ability to form new memories is reduced by 40% due to sleep deprivation.
- Sleep deprivation impairs the brain's processing of new experiences and the formation of memories.
- Quality sleep is essential for restoring and enhancing memory and learning capabilities.
- During deep sleep, sleep spindles assist in transferring memories from short-term to long-term storage.
- Disrupted deep sleep contributes to cognitive decline associated with aging and Alzheimer's disease.
- Understanding the physiology of sleep has the potential to improve memory, learning, and prevent cognitive decline.

## Question 5: What are some ideas that you can implement to improve your sleep quality?

**Answer 5:**

- Maintain a consistent sleep schedule.
- Establish a relaxing bedtime routine.
- Create a sleep-friendly environment (dark, quiet, and comfortable).
- Limit screen time before bed.
- Engage in regular exercise.
- Invest in a comfortable sleep environment (mattress, pillows, bedding).
- Manage stress and anxiety before bedtime.

## Question 6: Paraphrase the video "Brain Changing Benefits of Exercise." Minimum 5 points.

**Answer 6:**

- Engaging in physical activity has positive effects on the brain, protecting against conditions like depression, Alzheimer's, and dementia.
- Key brain regions involved in decision-making, attention, personality, and memory formation, such as the prefrontal cortex and hippocampus, benefit from regular exercise.
- A neuroscientist studying brain cells in the hippocampus recognized the need for changes due to a sedentary lifestyle.
- Incorporating regular exercise improved the speaker's mood, energy levels, and led to weight loss.
- Exercise also enhanced the speaker's focus and long-term memory.
- Scientific evidence supports the notion that physical activity has positive effects on mood, energy, memory, and attention.

## Question 7: What are some steps you can take to incorporate more exercise into your routine?

**Answer 7:**

- Set specific exercise goals.
- Start with small steps and gradually increase the intensity.
- Find enjoyable activities that make exercising fun.
- Schedule exercise into your daily routine.
- Exercise with a workout buddy or join a group for added motivation.
- Establish a consistent exercise routine.
- Incorporate variety into your workouts to keep things interesting.
- Set realistic expectations and be patient with your progress.
- Track your exercise progress to stay motivated.
- Seek rewards and support to maintain your motivation.
