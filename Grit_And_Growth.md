# Grit and Growth Mindset

## Question 1: Summarize the video in a few lines using your own words.

#### Answer 1:

* The video tells the story of a young teacher who noticed that students with lower IQ scores were outperforming some students with higher IQ scores.

* She studied psychology to understand the learning process and discovered that intelligence is not the sole determinant of success.

* She found that having grit and a growth mindset are important for learning, as they involve perseverance, effort, and the belief that abilities can be developed.

## Question 2: What are the key takeaways from the video that we can act on?

#### Answer 2:

* Grit and a growth mindset are crucial for achieving success in learning.

* We can change our mindset to foster growth and improve our learning abilities.

* Learning takes time and patience, so it's important to avoid rushing the process.

## Question 3: Summarize the video in a few lines using your own words.

#### Answer 3:

* The video explores the research of Carol Dweck, who studied why some equally talented individuals succeed while others do not.

* It introduces two mindsets: the fixed mindset and the growth mindset.

* The fixed mindset believes that abilities are innate and unchangeable, while the growth mindset believes that abilities can be developed over time.

* The video distinguishes between these mindsets and highlights the benefits of embracing a growth mindset.

## Question 4: What are the key takeaways from the video that we can act on?

#### Answer 4:

* By putting in effort and embracing a growth mindset, we can improve our learning abilities and experience personal growth.

* Prioritizing the learning process rather than focusing solely on outcomes is essential for long-term success.

* Challenges and mistakes are opportunities for learning and growth.

* Feedback plays a crucial role in fostering a growth mindset and continuous improvement.

## Question 5: What is the Internal Locus of Control? What is the key point in the video?

#### Answer 5:

* The internal locus of control refers to the belief that we have control over our own lives and outcomes.

* The video discusses different mindsets: the god-gifted mindset, the hard-working mindset, and the lazy mindset.

* It suggests that those with an internal locus of control, who are hard-working and enjoy challenges, are more motivated and likely to succeed.

## Question 6: Summarize the video in a few lines using your own words.

#### Answer 6:

* This video explores the concepts of acceptance mindset and unacceptance mindset. If we believe we have no control over our circumstances, we hinder our ability to change.

* However, by recognizing that we can improve and taking action, we can experience growth.

* A fixed mindset perceives oneself as unchangeable, leading to a lack of motivation for making improvements.

## Question 7: What are the key takeaways from the video that we can act on?

#### Answer 7:

* Embrace the belief in continuous improvement and growth throughout our lives.

* Challenge limiting assumptions to unlock our full potential.

* Create a personal life curriculum aligned with our passions and dreams.

* Value and learn from struggles and failures, nurturing our passion for ongoing growth.

## Question 8: What are one or more points that you want to take action on from the manual?

#### Answer 8:

* Take full responsibility for my learning journey.

* Assume ownership of assigned projects and ensure their execution, delivery, and functionality.

* Approach new challenges and interactions with enthusiasm and a positive attitude.

* Complete code only when it meets the following criteria:

   1. Functionality is ensured.
   
   2. Readability and understandability are prioritized.
   
   3. Optimization for efficiency is considered.
